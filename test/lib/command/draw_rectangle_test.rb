require 'minitest/autorun'
require_relative '../../../lib/command/draw_rectangle'

module Command
  class DrawRectangleTest < Minitest::Test
    
    def setup
      @bitmap = Bitmap.new(width: 3, height: 6)
    end
    
    def test_draw_rectangle_full_command
      raw_command = 'R 1 1 3 3 A'
      @bitmap = Command::DrawRectangle.new(bitmap: @bitmap, raw_command: raw_command).run
      assert_equal "AAA\nAOA\nAAA\nOOO\nOOO\nOOO", @bitmap.show
    end
    
    def test_draw_rectangle_full_command_at_a_point
      raw_command = 'R 1 1 1 1 A'
      @bitmap = Command::DrawRectangle.new(bitmap: @bitmap, raw_command: raw_command).run
      assert_equal "AOO\nOOO\nOOO\nOOO\nOOO\nOOO", @bitmap.show
    end
    
    def test_draw_rectangle_full_command_with_a_non_hollow_box
      raw_command = 'R 1 1 2 2 A'
      @bitmap = Command::DrawRectangle.new(bitmap: @bitmap, raw_command: raw_command).run
      assert_equal "AAO\nAAO\nOOO\nOOO\nOOO\nOOO", @bitmap.show
    end
    
    def test_draw_rectangle_full_command_with_a_line
      raw_command = 'R 1 1 1 3 A'
      @bitmap = Command::DrawRectangle.new(bitmap: @bitmap, raw_command: raw_command).run
      assert_equal "AOO\nAOO\nAOO\nOOO\nOOO\nOOO", @bitmap.show
    end
    
    def test_draw_rectangle_full_command_with_the_inverse_coordinates_at_two_by_tow
      raw_command = 'R 2 2 1 1 A'
      @bitmap = Command::DrawRectangle.new(bitmap: @bitmap, raw_command: raw_command).run
      assert_equal "AAO\nAAO\nOOO\nOOO\nOOO\nOOO", @bitmap.show
    end
    
    def test_draw_rectangle_full_command_with_the_inverse_coordinates_at_three_by_three
      raw_command = 'R 3 3 1 1 A'
      @bitmap = Command::DrawRectangle.new(bitmap: @bitmap, raw_command: raw_command).run
      assert_equal "AAA\nAOA\nAAA\nOOO\nOOO\nOOO", @bitmap.show
    end
    
    def test_draw_rectangle_full_command_with_the_full_grid_covered
      raw_command = 'R 1 1 3 6 A'
      @bitmap = Command::DrawRectangle.new(bitmap: @bitmap, raw_command: raw_command).run
      assert_equal "AAA\nAOA\nAOA\nAOA\nAOA\nAAA", @bitmap.show
    end
    
    def test_draw_rectangle_full_command_with_the_full_grid_covered_then_another_on_top
      first_command = 'R 1 1 3 6 A'
      @bitmap = Command::DrawRectangle.new(bitmap: @bitmap, raw_command: first_command).run
      assert_equal "AAA\nAOA\nAOA\nAOA\nAOA\nAAA", @bitmap.show

      second_command = 'R 1 1 3 3 B'
      @bitmap = Command::DrawRectangle.new(bitmap: @bitmap, raw_command: second_command).run
      assert_equal "BBB\nBOB\nBBB\nAOA\nAOA\nAAA", @bitmap.show
    end

    def test_draw_rectangl_command
      raw_command = 'R 1 1'
      error = assert_raises(CommandError) { Command::DrawRectangle.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'Draw rectangle command requires 6.', error.message
    end
    
    def test_draw_rectangle_with_invalid_numbers
      raw_command = 'R O O Y Y S'
      error = assert_raises(UnrecognisedNumber) { Command::DrawRectangle.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The number provided was not recognised, valid numbers are integers.', error.message
    end
    
    def test_draw_rectangle_with_invalid_colour
      raw_command = 'R 1 1 1 1 1'
      error = assert_raises(UnrecognisedColour) { Command::DrawRectangle.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message
    end
  end
end