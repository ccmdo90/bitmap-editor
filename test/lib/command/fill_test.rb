require 'minitest/autorun'
require_relative '../../../lib/command/fill'

module Command
  class FillTest < Minitest::Test
    def setup
      @bitmap = Bitmap.new(width: 2, height: 3)
    end

    def test_fill_command
      raw_command = 'F 1 1 F'
      @bitmap.colour(x: 2, y: 3, colour: 'X')
      @bitmap = Command::Fill.new(bitmap: @bitmap, raw_command: raw_command).run
      assert_equal "FF\nFF\nFX", @bitmap.show
    end

    def test_fill_with_wrong_number_of_args
      raw_command = 'F 1 1'
      error = assert_raises(CommandError) { Command::Fill.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'Fill command requires pixel coordinates, and a colour, eg F 1 2 R.', error.message
    end

    def test_fill_with_invalid_numbers
      raw_command = 'F A A R'
      error = assert_raises(UnrecognisedNumber) { Command::Fill.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The number provided was not recognised, valid numbers are integers.', error.message
    end

    def test_fill_with_invalid_colour
      raw_command = 'R 1 1 1'
      error = assert_raises(UnrecognisedColour) { Command::Fill.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message

      raw_command = 'R 1 1 a'
      error = assert_raises(UnrecognisedColour) { Command::Fill.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message

      raw_command = 'R 1 1 AB'
      error = assert_raises(UnrecognisedColour) { Command::Fill.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message
    end
  end
end
