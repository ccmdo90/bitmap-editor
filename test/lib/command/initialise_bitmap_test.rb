require 'minitest/autorun'
require_relative '../../../lib/command/initialise_bitmap'

module Command
  class InitialiseBitmapTest < Minitest::Test

    def test_initialize_command
      raw_command = 'I 2 2'
      @bitmap = Command::InitialiseBitmap.new(bitmap: nil, raw_command: raw_command).run
      assert_equal "OO\nOO", @bitmap.show
    end

    def test_initialize_command_with_wrong_number_of_args
      raw_command = 'I 2'
      error = assert_raises(CommandError) { Command::InitialiseBitmap.new(bitmap: nil, raw_command: raw_command).run }
      assert_equal 'Initialize command requires a width and height, e.g. I 5 5.', error.message
    end

    def test_initialize_command_with_invalid_numbers
      raw_command = 'I 2 H'
      error = assert_raises(UnrecognisedNumber) { Command::InitialiseBitmap.new(bitmap: nil, raw_command: raw_command).run }
      assert_equal 'The number provided was not recognised, valid numbers are integers.', error.message
    end
  end
end
