require 'minitest/autorun'
require_relative '../../../lib/command/draw_horizontal_line'

module Command
  class DrawVerticalLineTest < Minitest::Test
    def before
      @bitmap = Bitmap.new(width: 2, height: 3)
    end

    def test_draw_horizontal_line_command
      raw_command = 'H 1 2 1 G'
      @bitmap = Command::DrawHorizontalLine.new(bitmap: @bitmap, raw_command: raw_command).run
      assert_equal "GG\nOO\nOO", @bitmap.show
    end

    def test_draw_horizontal_line_with_wrong_number_of_args
      raw_command = 'H 1 1'
      error = assert_raises(CommandError) { Command::DrawHorizontalLine.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'Colour horizontal line command requires a row, pixel coordinates, and a colour, eg H x1 x2 y R.', error.message
    end

    def test_draw_horizontal_line_with_invalid_numbers
      raw_command = 'H O R U S'
      error = assert_raises(UnrecognisedNumber) { Command::DrawHorizontalLine.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The number provided was not recognised, valid numbers are integers.', error.message
    end

    def test_draw_horizontal_line_with_invalid_colour
      raw_command = 'H 1 2 1 1'
      error = assert_raises(UnrecognisedColour) { Command::DrawHorizontalLine.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message

      raw_command = 'H 1 2 1 a'
      error = assert_raises(UnrecognisedColour) { Command::DrawHorizontalLine.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message

      raw_command = 'H 1 2 1 AB'
      error = assert_raises(UnrecognisedColour) { Command::DrawHorizontalLine.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message
    end
  end
end
