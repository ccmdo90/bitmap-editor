require 'minitest/autorun'
require_relative '../../../lib/command/draw_vertical_line'

module Command
  class DrawVerticalLineTest < Minitest::Test
    def setup
      @bitmap = Bitmap.new(width: 2, height: 3)
    end

    def test_draw_vertical_line_command
      raw_command = 'V 1 1 2 G'
      @bitmap = Command::DrawVerticalLine.new(bitmap: @bitmap, raw_command: raw_command).run
      assert_equal "GO\nGO\nOO", @bitmap.show
    end
   
    def test_draw_vertical_line_with_wrong_number_of_args
      raw_command = 'V 1 1'
      error = assert_raises(CommandError) { Command::DrawVerticalLine.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'Colour vertical line command requires a column, pixel coordinates, and a colour, eg V x y1 y2 R.', error.message
    end

    def test_draw_vertical_line_with_invalid_numbers
      raw_command = 'V E N U S'
      error = assert_raises(UnrecognisedNumber) { Command::DrawVerticalLine.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The number provided was not recognised, valid numbers are integers.', error.message
    end

    def test_draw_vertical_line_with_invalid_colour
      raw_command = 'V 1 1 2 1'
      error = assert_raises(UnrecognisedColour) { Command::DrawVerticalLine.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message

      raw_command = 'V 1 1 2 a'
      error = assert_raises(UnrecognisedColour) { Command::DrawVerticalLine.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message

      raw_command = 'V 1 1 2 AB'
      error = assert_raises(UnrecognisedColour) { Command::DrawVerticalLine.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message
    end
  end
end
