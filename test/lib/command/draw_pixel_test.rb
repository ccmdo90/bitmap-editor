require 'minitest/autorun'
require_relative '../../../lib/command/draw_pixel'

module Command
  class DrawPixelTest < Minitest::Test
    def setup
      @bitmap = Bitmap.new(width: 2, height: 3)
    end

    def test_colour_command
      raw_command = 'L 1 1 G'
      @bitmap = Command::DrawPixel.new(bitmap: @bitmap, raw_command: raw_command).run
      assert_equal 'G', @bitmap[1, 1]
    end

    def test_colour_with_wrong_number_of_args
      raw_command = 'L 1 1'
      error = assert_raises(CommandError) { Command::DrawPixel.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'Colour pixel command requires pixel coordinates and a colour, eg L 2 5 R.', error.message
    end

    def test_colour_with_invalid_numbers
      raw_command = 'L A A G'
      error = assert_raises(UnrecognisedNumber) { Command::DrawPixel.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The number provided was not recognised, valid numbers are integers.', error.message
    end

    def test_colour_with_invalid_colour
      raw_command = 'L 1 1 1'
      error = assert_raises(UnrecognisedColour) { Command::DrawPixel.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message

      raw_command = 'L 1 1 a'
      error = assert_raises(UnrecognisedColour) { Command::DrawPixel.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message

      raw_command = 'L 1 1 AB'
      error = assert_raises(UnrecognisedColour) { Command::DrawPixel.new(bitmap: @bitmap, raw_command: raw_command).run }
      assert_equal 'The colour provided was not recognised, valid colours are uppercase A to Z.', error.message
    end
  end
end
