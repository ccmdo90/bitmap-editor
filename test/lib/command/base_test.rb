require 'minitest/autorun'
require_relative '../../../lib/command/base'

module Command
  class BaseTest < Minitest::Test
    def setup
      @base_command_class ||= Command::Base.new(bitmap: nil, raw_command: '')
    end

    def test_number_validation
      assert @base_command_class.send(:a_recognised_number?, number: '1')
      refute @base_command_class.send(:a_recognised_number?, number: 'A')
    end

    def test_colour_validation
      assert @base_command_class.send(:a_recognised_colour?, colour: 'A')
      refute @base_command_class.send(:a_recognised_colour?, colour: '1')
      refute @base_command_class.send(:a_recognised_colour?, colour: 'a')
      refute @base_command_class.send(:a_recognised_colour?, colour: 'AZ')
    end
  end
end
