require 'minitest/autorun'
require_relative '../../lib/bitmap'


class BitmapTest < Minitest::Test
  def test_bitmap_knows_its_dimensions
    bitmap = Bitmap.new(width: 1, height: 2)
    assert_equal 1, bitmap.width
    assert_equal 2, bitmap.height
  end

  def test_bitmap_ends_at_250_so_251_is_too_large
    assert_raises(BitmapTooLarge) { Bitmap.new(width: 1, height: 251) }
    error = assert_raises(BitmapTooLarge) { Bitmap.new(width: 251, height: 1) }
    assert_equal 'The coordinates provided were too large, valid coordinates are from 1 to 250.', error.message
  end

  def test_bitmap_starts_at_1_so_0_is_too_small
    assert_raises(BitmapTooSmall) { Bitmap.new(width: 250, height: 0) }
    error = assert_raises(BitmapTooSmall) { Bitmap.new(width: 0, height: 250) }
    assert_equal 'The coordinates provided were too small, valid coordinates are from 1 to 250.', error.message
  end

  def test_bitmap_with_no_height_supplied
    error = assert_raises(ArgumentError) { Bitmap.new(width: 1) }
    assert_equal 'missing keyword: height', error.message
  end

  def test_bitmap_with_no_width_supplied
    error = assert_raises(ArgumentError) { Bitmap.new(height: 1) }
    assert_equal 'missing keyword: width', error.message
  end

  def test_bitmap_data_after_initialization_at_any_pixel_is_O
    bitmap = Bitmap.new(width: 2, height: 2)
    assert_equal 'O', bitmap[1, 1]
    assert_equal 'O', bitmap[1, 2]
    assert_equal 'O', bitmap[2, 1]
    assert_equal 'O', bitmap[2, 2]
  end

  def test_bitmap_data_accessing_invalid_pixels
    bitmap = Bitmap.new(width: 2, height: 2)
    assert_raises(PixelOutOfBounds) { bitmap[0, 0] }
    assert_raises(PixelOutOfBounds) { bitmap[0, 1] }
    assert_raises(PixelOutOfBounds) { bitmap[1, 0] }
    assert_raises(PixelOutOfBounds) { bitmap[3, 1] }
    error = assert_raises(PixelOutOfBounds) { bitmap[1, 3] }
    assert_equal 'You tried to access a pixel that was out of bounds.', error.message
  end

  def test_colouring_a_pixel
    bitmap = Bitmap.new(width: 2, height: 2)
    bitmap.colour(x: 1, y: 1, colour: 'A')
    assert_equal 'A', bitmap[1, 1]
    assert_equal 'O', bitmap[1, 2]
    assert_equal 'O', bitmap[2, 1]
    assert_equal 'O', bitmap[2, 2]
  end

  def test_colouring_an_invalid_pixel
    bitmap = Bitmap.new(width: 2, height: 2)
    error = assert_raises(PixelOutOfBounds) { bitmap.colour(x: 0, y: 0, colour: 'A') }
    assert_equal 'You tried to access a pixel that was out of bounds.', error.message
  end

  def test_clearing_a_bitmap
    bitmap = Bitmap.new(width: 2, height: 2)
    bitmap.colour(x: 1, y: 1, colour: 'A')
    bitmap.clear
    assert_equal 'O', bitmap[1, 1]
  end

  def test_bitmap_can_show_itself_when_1_x_1
    bitmap = Bitmap.new(width: 1, height: 1)
    expected_output = "O"
    assert_equal expected_output, bitmap.show
  end

  def test_bitmap_can_show_itself
    bitmap = Bitmap.new(width: 2, height: 2)
    expected_output = "OO\nOO"
    assert_equal expected_output, bitmap.show
  end

  def test_bitmap_can_show_itself_with_uneven_edges
    bitmap = Bitmap.new(width: 1, height: 4)
    expected_output = "O\nO\nO\nO"
    assert_equal expected_output, bitmap.show
  end

  def test_bitmap_can_show_itself_with_colour
    bitmap = Bitmap.new(width: 2, height: 2)
    bitmap.colour(x: 1, y: 1, colour: 'Z')
    expected_output = "ZO\nOO"
    assert_equal expected_output, bitmap.show
  end

  def test_bitmap_colour_vertical_line_invalid_pixels
    bitmap = Bitmap.new(width: 2, height: 2)
    assert_raises(PixelOutOfBounds) { bitmap.colour_vertical_line(in_column: 0, from: 1, to: 2, colour: 'Z') }
    assert_raises(PixelOutOfBounds) { bitmap.colour_vertical_line(in_column: 1, from: 1, to: 3, colour: 'Z') }
    assert_raises(PixelOutOfBounds) { bitmap.colour_vertical_line(in_column: 1, from: 3, to: 1, colour: 'Z') }
  end

  def test_bitmap_colour_vertical_line
    bitmap = Bitmap.new(width: 2, height: 2)
    bitmap.colour_vertical_line(in_column: 1, from: 1, to: 2, colour: 'Z')
    expected_output = "ZO\nZO"
    assert_equal expected_output, bitmap.show
  end

  def test_bitmap_colour_vertical_line_backwards
    bitmap = Bitmap.new(width: 2, height: 2)
    bitmap.colour_vertical_line(in_column: 1, from: 2, to: 1, colour: 'Z')
    expected_output = "ZO\nZO"
    assert_equal expected_output, bitmap.show
  end

  def test_bitmap_colour_vertical_line_to_the_height_limit
    bitmap = Bitmap.new(width: 3, height: 4)
    bitmap.colour_vertical_line(in_column: 1, from: 1, to: 4, colour: 'H')
    expected_output = "HOO\nHOO\nHOO\nHOO"
    assert_equal expected_output, bitmap.show
  end

  def test_bitmap_colour_horizontal_line_invalid_pixels
    bitmap = Bitmap.new(width: 2, height: 2)
    assert_raises(PixelOutOfBounds) { bitmap.colour_horizontal_line(in_row: 0, from: 1, to: 2, colour: 'Z') }
    assert_raises(PixelOutOfBounds) { bitmap.colour_horizontal_line(in_row: 1, from: 1, to: 3, colour: 'Z') }
    assert_raises(PixelOutOfBounds) { bitmap.colour_horizontal_line(in_row: 1, from: 3, to: 1, colour: 'Z') }
  end

  def test_bitmap_colour_horizontal_line
    bitmap = Bitmap.new(width: 2, height: 2)
    bitmap.colour_horizontal_line(in_row: 1, from: 1, to: 2, colour: 'Z')
    expected_output = "ZZ\nOO"
    assert_equal expected_output, bitmap.show
  end

  def test_bitmap_colour_horizontal_line_backwards
    bitmap = Bitmap.new(width: 2, height: 2)
    bitmap.colour_horizontal_line(in_row: 1, from: 2, to: 1, colour: 'Z')
    expected_output = "ZZ\nOO"
    assert_equal expected_output, bitmap.show
  end

  def test_bitmap_colour_horizontal_line_to_the_width_limit
    bitmap = Bitmap.new(width: 4, height: 3)
    bitmap.colour_horizontal_line(in_row: 1, from: 1, to: 4, colour: 'H')
    expected_output = "HHHH\nOOOO\nOOOO"
    assert_equal expected_output, bitmap.show
  end

  def test_fill_an_invalid_pixel
    bitmap = Bitmap.new(width: 2, height: 2)
    error = assert_raises(PixelOutOfBounds) { bitmap.fill(x: 0, y: 0, colour: 'A') }
    assert_equal 'You tried to access a pixel that was out of bounds.', error.message
  end

  def test_fill_a_1_x_1_bitmap
    bitmap = Bitmap.new(width: 1, height: 1)
    bitmap.fill(x: 1, y: 1, colour: 'A')
    assert_equal 'A', bitmap.show
  end
  
  def test_fill_a_1_x_2_bitmap
    bitmap = Bitmap.new(width: 1, height: 2)
    bitmap.fill(x: 1, y: 1, colour: 'A')
    assert_equal "A\nA", bitmap.show
  end
  
  def test_fill_a_1_x_2_bitmap_where_there_are_two_different_colours
    bitmap = Bitmap.new(width: 1, height: 2)
    bitmap.colour(x: 1, y: 2, colour: 'X')
    bitmap.fill(x: 1, y: 1, colour: 'A')
    assert_equal "A\nX", bitmap.show
  end
  
  def test_fill_a_1_x_3_bitmap_with_a_different_colour_in_the_middle
    bitmap = Bitmap.new(width: 1, height: 3)
    bitmap.colour(x: 1, y: 2, colour: 'X')
    bitmap.fill(x: 1, y: 1, colour: 'A')
    assert_equal "A\nX\nO", bitmap.show
  end
  
  def test_fill_a_snaking_pattern
    bitmap = Bitmap.new(width: 5, height: 5)
    bitmap.colour_horizontal_line(in_row: 2, from: 1, to: 4, colour: 'Z')
    bitmap.colour_horizontal_line(in_row: 4, from: 2, to: 5, colour: 'Z')
    bitmap.fill(x: 1, y: 1, colour: 'A')
    assert_equal "AAAAA\nZZZZA\nAAAAA\nAZZZZ\nAAAAA", bitmap.show
  end
  
  def test_fill_doesnt_cross_diagonals
    bitmap = Bitmap.new(width: 2, height: 2)
    bitmap.colour(x: 1, y: 1, colour: 'A')
    bitmap.colour(x: 2, y: 2, colour: 'A')
    bitmap.fill(x: 1, y: 1, colour: 'A')
    assert_equal "AO\nOA", bitmap.show
  end
end
