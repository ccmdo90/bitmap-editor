require 'minitest/autorun'
require_relative '../../lib/bitmap_editor'

class BitmapEditorTest < Minitest::Test

  def test_run_with_a_file_that_doesnt_exist
    error = assert_raises(FileNotFound) { BitmapEditor.new(file_path: 'doesnt-exist.txt').run }
    assert_equal 'The file was not found.', error.message
  end

  def test_run_with_a_file_that_is_empty
    assuming_file_with(content: nil) do
      error = assert_raises(FileEmpty) { BitmapEditor.new(file_path: test_file_name).run }
      assert_equal 'The file was empty.', error.message
    end

    assuming_file_with(content: '') do
      error = assert_raises(FileEmpty) { BitmapEditor.new(file_path: test_file_name).run }
      assert_equal 'The file was empty.', error.message
    end
  end

  def test_run_without_an_initialise_command
    assuming_file_with(content: "S\n") do
      error = assert_raises(NotInitialised) { BitmapEditor.new(file_path: test_file_name).run }
      assert_equal 'File must start with an initialize command, e.g. I 5 5.', error.message
    end
  end

  def test_integration_without_a_clear
    assuming_file_with(content: "I 5 6\nL 1 3 A\nV 2 3 6 W\nH 3 5 2 Z\n") do
      bitmap_editor = BitmapEditor.new(file_path: test_file_name)
      bitmap_editor.run
      assert_equal "OOOOO\nOOZZZ\nAWOOO\nOWOOO\nOWOOO\nOWOOO", bitmap_editor.bitmap.show
    end
  end

  def test_integration_with_a_clear
    assuming_file_with(content: "I 5 6\nL 1 3 A\nV 2 3 6 W\nH 3 5 2 Z\nC\n") do
      bitmap_editor = BitmapEditor.new(file_path: test_file_name)
      bitmap_editor.run
      assert_equal "OOOOO\nOOOOO\nOOOOO\nOOOOO\nOOOOO\nOOOOO", bitmap_editor.bitmap.show
    end
  end

  def test_notimplemented_command
    assuming_file_with(content: "I 5 6\nU\n") do
      error = assert_raises(NotImplemented) { BitmapEditor.new(file_path: test_file_name).run }
      assert_equal 'Could not determine which command to run, valid commands are I, L, V, H, C, and S', error.message
    end
  end
  
  def test_draw_rectangle_integration
    assuming_file_with(content: "I 3 6\nR 1 1 3 3 A\n") do
      bitmap_editor = BitmapEditor.new(file_path: test_file_name)
      bitmap_editor.run
      assert_equal "AAA\nAOA\nAAA\nOOO\nOOO\nOOO", bitmap_editor.bitmap.show
    end
  end

  private

  def test_file_name
    'test_file.txt'
  end

  def assuming_file_with content:, &block
    File.open(test_file_name, 'w+') { |file| file.write(content) }
    yield
  ensure
    File.delete(test_file_name)
  end
end
