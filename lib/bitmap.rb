# frozen_string_literal: true

class Bitmap
  attr_reader :width, :height

  def initialize width:, height:
    raise BitmapTooLarge.new if [width, height].max > 250
    raise BitmapTooSmall.new if [width, height].min <= 0
    @width = width
    @height = height
  end

  def data
    @data ||= Hash.new('O')
  end

  def [] x, y
    raise PixelOutOfBounds.new if out_of_bounds?(x: x, y: y)
    data[[x, y]]
  end

  def colour x:, y:, colour:
    raise PixelOutOfBounds.new if out_of_bounds?(x: x, y: y)
    data[[x, y]] = colour
  end

  def colour_vertical_line in_column:, from:, to:, colour:
    raise PixelOutOfBounds.new if out_of_bounds?(x: in_column, y: from)
    raise PixelOutOfBounds.new if out_of_bounds?(x: in_column, y: to)
    if from < to
      from.upto(to) { |row| colour(x: in_column, y: row, colour: colour) }
    else
      from.downto(to) { |row| colour(x: in_column, y: row, colour: colour) }
    end
  end

  def colour_horizontal_line in_row:, from:, to:, colour:
    raise PixelOutOfBounds.new if out_of_bounds?(x: from, y: in_row)
    raise PixelOutOfBounds.new if out_of_bounds?(x: to, y: in_row)
    if from < to
      from.upto(to) { |column| colour(x: column, y: in_row, colour: colour) }
    else
      from.downto(to) { |column| colour(x: column, y: in_row, colour: colour) }
    end
  end

  def fill x:, y:,colour:, target_colour: nil
    raise PixelOutOfBounds.new if target_colour.nil? && out_of_bounds?(x: x, y: y)
    return if out_of_bounds?(x: x, y: y)
    target_colour ||= data[[x, y]]

    return unless data[[x, y]] == target_colour

    data[[x, y]] = colour

    fill(x: x - 1, y: y, colour: colour, target_colour: target_colour)
    fill(x: x + 1, y: y, colour: colour, target_colour: target_colour)
    fill(x: x, y: y - 1, colour: colour, target_colour: target_colour)
    fill(x: x, y: y + 1, colour: colour, target_colour: target_colour)
  end
  
  def draw_rectangle x1:, y1:, x2:, y2:, colour:
    colour_horizontal_line(in_row: y1, from: x1, to: x2, colour: colour)
    colour_horizontal_line(in_row: y2, from: x1, to: x2, colour: colour)
    colour_vertical_line(in_column: x1, from: y1, to: y2, colour: colour)
    colour_vertical_line(in_column: x2, from: y1, to: y2, colour: colour)
  end

  def clear
    @data = Hash.new('O')
  end

  def show
    show_string = ''
    (1..height).each do |row|
      (1..width).each do |column|
        show_string += data[[column, row]]
      end
      show_string += "\n"
    end
    show_string[0..-2]
  end

  private

  def out_of_bounds? x:, y:
    [x, y].min < 1 || x > width || y > height
  end
end

class BitmapError < StandardError; end

class BitmapTooLarge < BitmapError
  def initialize message = 'The coordinates provided were too large, valid coordinates are from 1 to 250.'
    super
  end
end

class BitmapTooSmall < BitmapError
  def initialize message = 'The coordinates provided were too small, valid coordinates are from 1 to 250.'
    super
  end
end

class PixelOutOfBounds < BitmapError
  def initialize message = 'You tried to access a pixel that was out of bounds.'
    super
  end
end
