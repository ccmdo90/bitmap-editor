# frozen_string_literal: true

require_relative 'base'

module Command
  class InitialiseBitmap < Base
    VALID_COMMAND_LENGTH = 3
    INVALID_ARG_LENGTH_MSG = 'Initialize command requires a width and height, e.g. I 5 5.'

    def run
      validate_args
      Bitmap.new(width: command[1].to_i, height: command[2].to_i)
    end

    private

    def validate_args
      raise CommandError.new(INVALID_ARG_LENGTH_MSG) unless command.length == VALID_COMMAND_LENGTH
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[1])
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[2])
    end
  end
end
