# frozen_string_literal: true

require_relative 'base'

module Command
  class DrawHorizontalLine < Base
    VALID_COMMAND_LENGTH = 5
    INVALID_ARG_LENGTH_MSG = 'Colour horizontal line command requires a row, pixel coordinates, and a colour, eg H x1 x2 y R.'

    def run
      validate_args
      bitmap.colour_horizontal_line(in_row: command[3].to_i, from: command[1].to_i, to: command[2].to_i, colour: command[4])
      bitmap
    end

    private

    def validate_args
      raise CommandError.new(INVALID_ARG_LENGTH_MSG) unless command.length == VALID_COMMAND_LENGTH
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[1])
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[2])
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[3])
      raise UnrecognisedColour.new unless a_recognised_colour?(colour: command[4])
    end
  end
end
