# frozen_string_literal: true

require_relative 'base'

module Command
  class Show < Base
    def run
      puts bitmap.show
      bitmap
    end
  end
end
