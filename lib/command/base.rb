# frozen_string_literal: true

module Command
  class Base
    attr_reader :bitmap, :command

    def initialize bitmap:, raw_command:
      @bitmap = bitmap
      @command = parse(raw_command: raw_command)
    end

    private

    def parse raw_command:
      raw_command.split
    end

    def a_recognised_number? number:
      number.to_i.to_s == number
    end

    def a_recognised_colour? colour:
      colour.size == 1 && colour.is_a?(String) && colour.match(/[A-Z]/)
    end
  end
end

class CommandError < StandardError; end

class UnrecognisedNumber < CommandError
  def initialize message = 'The number provided was not recognised, valid numbers are integers.'
    super
  end
end

class UnrecognisedColour < CommandError
  def initialize message = 'The colour provided was not recognised, valid colours are uppercase A to Z.'
    super
  end
end
