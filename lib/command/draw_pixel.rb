# frozen_string_literal: true

require_relative 'base'

module Command
  class DrawPixel < Base
    VALID_COMMAND_LENGTH = 4
    INVALID_ARG_LENGTH_MSG = 'Colour pixel command requires pixel coordinates and a colour, eg L 2 5 R.'

    def run
      validate_args
      bitmap.colour(x: command[1].to_i, y: command[2].to_i, colour: command[3])
      bitmap
    end

    private

    def validate_args
      raise CommandError.new(INVALID_ARG_LENGTH_MSG) unless command.length == VALID_COMMAND_LENGTH
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[1])
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[2])
      raise UnrecognisedColour.new unless a_recognised_colour?(colour: command[3])
    end
  end
end
