# frozen_string_literal: true

require_relative 'base'

module Command
  class DrawRectangle < Base
    VALID_COMMAND_LENGTH = 6
    INVALID_ARG_LENGTH_MSG = 'Draw rectangle command requires 6.'
    
    def run
      validate_args
      bitmap.draw_rectangle(x1: command[1].to_i, y1: command[2].to_i, x2: command[3].to_i, y2: command[4].to_i, colour: command[5])
      bitmap
    end
    
    private

    def validate_args
      raise CommandError.new(INVALID_ARG_LENGTH_MSG) unless command.length == VALID_COMMAND_LENGTH
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[1])
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[2])
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[3])
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[4])
      raise UnrecognisedColour.new unless a_recognised_colour?(colour: command[5])
    end
  end
end