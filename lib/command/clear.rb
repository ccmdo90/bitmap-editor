# frozen_string_literal: true

require_relative 'base'

module Command
  class Clear < Base
    def run
      bitmap.clear
      bitmap
    end
  end
end
