# frozen_string_literal: true

require_relative 'base'

module Command
  class Fill < Base
    VALID_COMMAND_LENGTH = 4
    INVALID_ARG_LENGTH_MSG = 'Fill command requires pixel coordinates, and a colour, eg F 1 2 R.'

    def run
      validate_args
      bitmap.fill(x: command[1].to_i, y: command[2].to_i, colour: command[3])
      bitmap
    end

    private

    def validate_args
      raise CommandError.new(INVALID_ARG_LENGTH_MSG) unless command.length == VALID_COMMAND_LENGTH
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[1])
      raise UnrecognisedNumber.new unless a_recognised_number?(number: command[2])
      raise UnrecognisedColour.new unless a_recognised_colour?(colour: command[3])
    end
  end
end
