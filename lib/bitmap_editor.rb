# frozen_string_literal: true

require_relative 'bitmap'
require_relative 'command/initialise_bitmap'
require_relative 'command/draw_pixel'
require_relative 'command/draw_vertical_line'
require_relative 'command/draw_horizontal_line'
require_relative 'command/fill'
require_relative 'command/draw_rectangle'
require_relative 'command/show'
require_relative 'command/clear'

class BitmapEditor
  attr_reader :raw_command_lines_from_file, :bitmap

  COMMANDS = {
    'I' => Command::InitialiseBitmap,
    'L' => Command::DrawPixel,
    'V' => Command::DrawVerticalLine,
    'H' => Command::DrawHorizontalLine,
    'F' => Command::Fill,
    'R' => Command::DrawRectangle,
    'S' => Command::Show,
    'C' => Command::Clear
  }

  def initialize file_path:
    raise FileNotFound unless File.exist?(file_path)
    raise FileEmpty if File.read(file_path).empty?
    @raw_command_lines_from_file = File.open(file_path).readlines
  end

  def run
    raise NotInitialised unless first_command_is_an_initialiser?
    raw_command_lines_from_file.each do |raw_command|
      @bitmap = command_runner.fetch(raw_command[0]).new(bitmap: bitmap, raw_command: raw_command).run
    end
  rescue KeyError
    raise NotImplemented
  end

  def command_runner
    @runner ||= Hash.new(NotImplemented).merge(COMMANDS)
  end

  private

  def first_command_is_an_initialiser?
    first_command = raw_command_lines_from_file.first.chr
    command_runner[first_command] == Command::InitialiseBitmap
  end
end

class FileNotFound < StandardError
  def initialize message = 'The file was not found.'
    super
  end
end

class FileEmpty < StandardError
  def initialize message = 'The file was empty.'
    super
  end
end

class NotInitialised < StandardError
  def initialize message = 'File must start with an initialize command, e.g. I 5 5.'
    super
  end
end

class NotImplemented < StandardError
  def initialize message = 'Could not determine which command to run, valid commands are I, L, V, H, C, and S'
    super
  end
end